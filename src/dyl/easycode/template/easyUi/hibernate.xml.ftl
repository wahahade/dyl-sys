<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE hibernate-mapping PUBLIC "-//Hibernate/Hibernate Mapping DTD 3.0//EN"
"http://hibernate.sourceforge.net/hibernate-mapping-3.0.dtd">
<!-- 由dyl EasyCode自动生成   ${sysTime}-->
<hibernate-mapping>
    <class name="${table.packageName}.${table.className}" table="${table.tableName}"   dynamic-insert="true">
    	 <#list table.primaryKeys as c>
    	 	<!--${c.remarks!''}-->
	        <id name="${c.javaProperty}" type="${c.javaTypeInPackAge}">
	            <column name="TB_ID" precision="22" scale="0" />
	            <generator class="sequence">
	            	<param name="sequence">SEQ_TB_ID</param>
	            </generator>
       		 </id>
        </#list>
        <#list table.baseColumns as c>
        	<!--${c.remarks!''}-->
	        <property name="${c.javaProperty}" type="${c.javaTypeInPackAge}">
	            <column name="${c.columnName}" ${c.nullable?string('','not-null="true"')} <#if c.size!=4000>length="${c.size}"</#if> />
	        </property>
		</#list>
        
    </class>
</hibernate-mapping>