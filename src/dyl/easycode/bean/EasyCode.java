/**
 * 由 EasyCode自动生成
 * @author Dyl
 * 2017-05-31 15:06:00
*/
package dyl.easycode.bean;

import java.io.Serializable;

import com.alibaba.fastjson.annotation.JSONField;

public class EasyCode implements Serializable {
    private static final long serialVersionUID = 1L;
	/*字段说明：
	*对应db字段名:name 类型:VARCHAR2(30)  
	*是否可以为空:是
	*/
	
	private  String  name;
	
	private  String pKey;
	private  String[] templates;
	private String hasEdit;
	private String hasDelete;
	
	
    public String getpKey() {
		return pKey;
	}
	public void setpKey(String pKey) {
		this.pKey = pKey;
	}
	public String[] getTemplates() {
		return templates;
	}
	public void setTemplates(String[] templates) {
		this.templates = templates;
	}
	public String getHasEdit() {
		return hasEdit;
	}
	public void setHasEdit(String hasEdit) {
		this.hasEdit = hasEdit;
	}
	public String getHasDelete() {
		return hasDelete;
	}
	public void setHasDelete(String hasDelete) {
		this.hasDelete = hasDelete;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
	public String toString() {
		return 
		"name:"+name+"\n"+
		"";
	}
}
