package dyl.test;


import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import dyl.sys.Annotation.Auth;
import dyl.sys.action.SysUserAction;

public class IntactCheckUtil {

    public static boolean check(Object obj){
           // list = Arrays.asList(AnnotationParsing.class.getClassLoader().loadClass(((Class)obj).getClass().getName()).getDeclaredFields());
        List<Field>  list = Arrays.asList(obj.getClass().getDeclaredFields());
        for(int i=0;i<list.size();i++){
            Field field = list.get(i);
            if(field.isAnnotationPresent(MyAnno.class)){//是否使用MyAnno注解
                for (Annotation anno : field.getDeclaredAnnotations()) {//获得所有的注解
                    if(anno.annotationType().equals(MyAnno.class) ){//找到自己的注解
                    	//System.out.println((MyAnno)anno).isCanNull());
                    	//if(!((MyAnno)anno).isCanNull()){
                    		System.out.println(!((MyAnno)anno).isCanNull());
                    	//}
                       /* if(!((MyAnno)anno).isCanNull()){//注解的值
                            if(TestUtil.getFieldValueByName(field.getName(),obj)==null){
                                throw new RuntimeException("类："+Mouse.class+"的属性："+field.getName()+"不能为空，实际的值:"+TestUtil.getFieldValueByName(field.getName(),obj)+",注解：field.getDeclaredAnnotations()");
                            }
                        }
                        if(!((MyAnno)anno).isCanEmpty()){
                            if(TestUtil.getFieldValueByName(field.getName(),obj).equals("")){
                                throw new RuntimeException("类："+Mouse.class+"的属性："+field.getName()+"不能为空字符串，实际的值:"+TestUtil.getFieldValueByName(field.getName(),obj)+",注解：field.getDeclaredAnnotations()");
                            }
                        }
                        if(!((MyAnno)anno).isCanZero()){
                            if(TestUtil.getFieldValueByName(field.getName(),obj).toString().equals("0") || TestUtil.getFieldValueByName(field.getName(),obj).toString().equals("0.0")){
                                throw new RuntimeException("类："+Mouse.class+"的属性："+field.getName()+"不能为空字符0，实际的值:"+TestUtil.getFieldValueByName(field.getName(),obj)+",注解：field.getDeclaredAnnotations()");
                            }
                        }*/
                    }
                }
            }

        }
        return  true;
    }
    public static boolean check2(Object obj){
     List<Method>  list = Arrays.asList(obj.getClass().getDeclaredMethods());
     for(int i=0;i<list.size();i++){
    	 Method method = list.get(i);
         if(method.isAnnotationPresent(Auth.class)){//是否使用MyAnno注解
             for (Annotation anno : method.getDeclaredAnnotations()) {//获得所有的注解
                 if(anno.annotationType().equals(Auth.class) ){//找到自己的注解
                	 System.out.println(anno.toString()+","+method.getName());
                	 System.out.println(((Auth)anno).action().getIndex());
                 }
             }
         }
     }
     return  true;
    }
    public static void main(String[] args) {
    	check2(new SysUserAction());
	}
}