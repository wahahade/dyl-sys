package dyl.sys.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import dyl.common.exception.CommonException;
import dyl.common.util.ConfigUtil;
import dyl.common.util.JdbcTemplateUtil;
import dyl.sys.Annotation.Auth;
import dyl.sys.service.SysRoleServiceImpl;
import dyl.sys.service.SysRoleUserServiceImpl;
import dyl.sys.service.SysUserServiceImpl;
/**
 * 由dyl EasyCode自动生成
 * @author Dyl
 * 2017-03-22 21:57:10
 */
@Controller
public class SysLogAction extends BaseAction{
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	@Resource
	private SysUserServiceImpl sysUserServiceImpl;
	@Resource
	private SysRoleServiceImpl sysRoleServiceImpl;
	@Resource
	private SysRoleUserServiceImpl sysRoleUserServiceImpl;
	 /**
	 * 说明：进入主页面方法
	 * @return String
	 */
	@Auth
	@RequestMapping(value = "/log!main.do")
	public String  main(HttpServletRequest request){
		try {
			File file=new File(ConfigUtil.getValue("tomcatLogPath"));
	    	File[] tempList = file.listFiles();
	    	List<Map<String,String>> fileList = new ArrayList<Map<String,String>>();
	    	for (int i = 0; i < tempList.length; i++) {
				if(tempList[i].isDirectory()){
					for (int j = 0; j < tempList[i].listFiles().length; j++) {
						Map<String,String> map = new HashMap<String, String>();
						map.put("fileName",tempList[i].listFiles()[j].getName());
						map.put("filePath", tempList[i].listFiles()[j].getAbsolutePath());
						fileList.add(map);
					}
				}else{
					Map<String,String> map = new HashMap<String, String>();
					map.put("fileName",tempList[i].getName());
					map.put("filePath", tempList[i].getAbsolutePath());
					fileList.add(map);
				}
			}
	    	request.setAttribute("fileList", fileList);
		} catch (Exception e) {
			throw new CommonException(request, e);
		}
		return "/sys/log/logMain";
	}
	/**
	 * 下载单个文件
	 * @param fileName
	 * @param name
	 * @param request
	 * @param response
	 * @throws Exception
	 */
    @RequestMapping(value = "/log!download.do")
	public void downfile(String filePath,String fileName,HttpServletRequest request,HttpServletResponse response) throws Exception{
	    BufferedInputStream bis = null;
	    BufferedOutputStream bos = null;
	    OutputStream fos = null;
	    InputStream fis = null;
        File downFiles = new File(filePath);
        if(!downFiles.exists()){
        	response.getWriter().print("文件不存在");
            return;
        }
        try {
            fis = new FileInputStream(downFiles);
            bis = new BufferedInputStream(fis);
            fos = response.getOutputStream();
            bos = new BufferedOutputStream(fos);
            setFileDownloadHeader(request,response,fileName);
            int byteRead = 0;
            byte[] buffer = new byte[8192];
            while((byteRead=bis.read(buffer,0,8192))!=-1){
                bos.write(buffer,0,byteRead);
            }
            bos.flush();
            fis.close();
            bis.close();
            fos.close();
            bos.close();
        } catch (Exception e) {
            log.info("下载失败了......");
        }
	}
	/**
	 * 设置让浏览器弹出下载对话框的Header.
	 * 根据浏览器的不同设置不同的编码格式  防止中文乱码
	 * @param fileName 下载后的文件名.
	 */
	public static void setFileDownloadHeader(HttpServletRequest request,HttpServletResponse response, String fileName) {
	    try {
	        //中文文件名支持
	        String encodedfileName = null;
	        String agent = request.getHeader("USER-AGENT");
	        //IE11 使用 Gecko, 不大于10使用 MSIE
			if(null != agent && (-1 != agent.indexOf("MSIE") || -1 != agent.indexOf("like Gecko"))){//IE
	            encodedfileName = java.net.URLEncoder.encode(fileName,"UTF-8");
	        }else if(null != agent && -1 != agent.indexOf("Mozilla")){
	            encodedfileName = new String (fileName.getBytes("UTF-8"),"iso-8859-1");
	        }else{
	            encodedfileName = java.net.URLEncoder.encode(fileName,"UTF-8");
	        }
	        response.setHeader("Content-Disposition", "attachment; filename=\"" + encodedfileName + "\"");
	    } catch (UnsupportedEncodingException e) {
	        e.printStackTrace();
	    }
	}
}
