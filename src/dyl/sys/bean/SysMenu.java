/**
 * 由 EasyCode自动生成
 * @author Dyl
 * 2017-03-15 15:49:48
*/
package dyl.sys.bean;

import java.io.Serializable;
import com.alibaba.fastjson.annotation.JSONField;
import java.math.BigDecimal;
import java.util.Date;

public class SysMenu implements Serializable {
    private static final long serialVersionUID = 1L;
	/*字段说明：
	*对应db字段名:id 类型:NUMBER(22) 主键 
	*是否可以为空:否
	*/
	
	private  BigDecimal  id;
	
	/*字段说明：父Id
	*对应db字段名:pid 类型:NUMBER(22)  
	*是否可以为空:是
	*/
	
	private  BigDecimal  pid;
	
	/*字段说明：排序
	*对应db字段名:oid 类型:NUMBER(22)  
	*是否可以为空:是
	*/
	
	private  BigDecimal  oid;
	
	/*字段说明：标题
	*对应db字段名:title 类型:VARCHAR2(50)  
	*是否可以为空:是
	*/
	
	private  String  title;
	
	/*字段说明：备注
	*对应db字段名:note 类型:VARCHAR2(100)  
	*是否可以为空:是
	*/
	
	private  String  note;
	
	/*字段说明：状态,0正常，1禁用
	*对应db字段名:state 类型:CHAR(1)  
	*是否可以为空:是
	*/
	
	private  String  state;
	
	/*字段说明：链接
	*对应db字段名:url 类型:VARCHAR2(100)  
	*是否可以为空:是
	*/
	@JSONField(name="href")
	private  String  url;
	
	/*字段说明：图标
	*对应db字段名:icon 类型:VARCHAR2(100)  
	*是否可以为空:是
	*/
	
	private  String  icon;
	
	/*字段说明：创建时间
	*对应db字段名:create_time 类型:DATE(7)  
	*是否可以为空:是
	*/
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private  Date  createTime;
	
	/*字段说明：创建者
	*对应db字段名:creator 类型:NUMBER(22)  
	*是否可以为空:是
	*/
	
	private  BigDecimal  creator;
	
	
	private String actionClass;
	
	public void setActionClass(String actionClass) {
		this.actionClass = actionClass;
	}
	public String getActionClass() {
		return actionClass;
	}
    public BigDecimal getId() {
        return id;
    }
    public void setId(BigDecimal id) {
        this.id = id;
    }
    public BigDecimal getPid() {
        return pid;
    }
    public void setPid(BigDecimal pid) {
        this.pid = pid;
    }
    public BigDecimal getOid() {
        return oid;
    }
    public void setOid(BigDecimal oid) {
        this.oid = oid;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getNote() {
        return note;
    }
    public void setNote(String note) {
        this.note = note;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getIcon() {
        return icon;
    }
    public void setIcon(String icon) {
        this.icon = icon;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public BigDecimal getCreator() {
        return creator;
    }
    public void setCreator(BigDecimal creator) {
        this.creator = creator;
    }
	public String toString() {
		return 
		"id:"+id+"\n"+
		
		"pid:"+pid+"\n"+
		
		"oid:"+oid+"\n"+
		
		"title:"+title+"\n"+
		
		"note:"+note+"\n"+
		
		"state:"+state+"\n"+
		
		"url:"+url+"\n"+
		
		"icon:"+icon+"\n"+
		
		"createTime:"+createTime+"\n"+
		
		"creator:"+creator+"\n"+
		"";
	}
}
