/**
 * 由 EasyCode自动生成
 * @author Dyl
 * 2017-04-14 17:37:34
*/
package dyl.sys.bean;

import java.io.Serializable;
import com.alibaba.fastjson.annotation.JSONField;
import java.math.BigDecimal;

public class SysAuthKind implements Serializable {
    private static final long serialVersionUID = 1L;
	/*字段说明：种类ID
	*对应db字段名:id 类型:NUMBER(22) 主键 
	*是否可以为空:是
	*/
	
	private  BigDecimal  id;
	
	/*字段说明：种类名称
	*对应db字段名:name 类型:VARCHAR2(20)  
	*是否可以为空:是
	*/
	
	private  String  name;
	
	private  BigDecimal ck;//是否选中
	
	public BigDecimal getCk() {
		return ck;
	}
	public void setCk(BigDecimal ck) {
		this.ck = ck;
	}
    public BigDecimal getId() {
        return id;
    }
    public void setId(BigDecimal id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
	public String toString() {
		return 
		"id:"+id+"\n"+
		
		"name:"+name+"\n"+
		"";
	}
}
