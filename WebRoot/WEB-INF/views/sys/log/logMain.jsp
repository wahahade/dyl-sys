<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>用户管理</title>
		<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
	</head>
	<body>
		<div class="admin-main">
			<!-- 查询条件 -->
			<form  class="layui-elem-quote" action="sysUser!main.do" id="mainForm" method="post">
				<div class="layui-fr">
					<a href="javascript:;" class="layui-btn layui-btn-small" id="search">
						<i class="layui-icon">&#xe615;</i> 查询
					</a>
				</div>
				<div class="layui-input-inline">
			       <label class="layui-form-label">用户名:</label>
			       <div class="layui-input-inline">
			       		  <input type="text" name="username"  value="${sysUser.username}" placeholder="请输入用户名" class="layui-input">
			       </div>
				</div>
			</form>
			<!-- 主table -->
			<fieldset class="layui-elem-field" >
				<legend>数据列表</legend>
				<div class="layui-field-box layui-form">
					<table class="layui-table admin-table">
						<thead>
							<tr>
								<th>日志名称</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${fileList}" var="f">
								<tr>
							        <td>${f.fileName}</td>
							        <td>
										<a href="log!download.do?filePath=${f.filePath}&fileName=${f.fileName}" class="layui-btn layui-btn-mini" data-id="${u.id}" data-opt="edit">
											<i class="layui-icon">&#xe642;</i> 下载
										</a>
									</td>
							    </tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</fieldset>
			<!-- 分页div -->
			<div class="admin-table-page">
				<div id="page" class="page">
				</div>
			</div>
		</div>
		<!-- 通用js -->
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<script>
		</script>
	</body>
</html>