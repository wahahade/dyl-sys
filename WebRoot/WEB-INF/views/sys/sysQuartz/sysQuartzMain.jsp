<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>定时任务配置</title>
		<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
	</head>

	<body>
		<div class="admin-main">
			<!-- 查询条件 -->
			<form  class="layui-elem-quote" action="sysQuartz!main.do" id="mainForm" method="post">
				<div class="layui-fr">
					<a href="javascript:;" class="layui-btn layui-btn-small" id="search">
						<i class="layui-icon">&#xe615;</i> 查询
					</a>
					<a href="javascript:;" class="layui-btn layui-btn-small" id="add">
						<i class="layui-icon">&#xe608;</i> 添加
					</a>
					<a href="javascript:;" class="layui-btn layui-btn-small" id="delete">
						<i class="layui-icon">&#xe640;</i> 删除
					</a>
				</div>
    			<div class="layui-input-inline">
			        <label class="layui-form-label">触发器名称:</label>
			        <div class="layui-input-inline">
			       		  <input type="text" name="triggername"  value="${sysQuartz.triggername}" placeholder="请输入触发器名称" class="layui-input">
			        </div>
			    </div>
			</form>
			<!-- 主table -->
			<fieldset class="layui-elem-field" >
				<legend>数据列表</legend>
				<div class="layui-field-box layui-form">
					<table class="layui-table admin-table">
						<thead>
							<tr>
								<th class="table-check"><input type="checkbox" name="" lay-skin="primary" lay-filter="allChoose"></th>
								<th>触发器名称</th>
								<th>时间表达式</th>
								<th>脚本名称</th>
								<th>目标类</th>
								<th>方法名</th>
								<th>是否并发启动任务</th>
								<th>状态</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${sysQuartzList}" var="o">
								<tr>
							        <td><input type="checkbox" lay-skin="primary" data-opt="check" data-id="${o.id}"></td>
									<td>${o.triggername}</td>
									<td>${o.cronexpression}</td>
									<td>${o.jobdetailname}</td>
									<td>${o.targetobject}</td>
									<td>${o.methodname}</td>
									<td>${o.concurrent==1?'并发':'非并发'}</td>
									<td>${o.state == 1? '启用':'关闭'}</td>
							        <td>
										<a href="javascript:;" class="layui-btn layui-btn-mini" data-id="${o.id}" data-opt="edit">
											<i class="layui-icon">&#xe642;</i> 修改
										</a>
									</td>
							    </tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</fieldset>
			<!-- 分页div -->
			<div class="admin-table-page">
				<div id="page" class="page">
				</div>
			</div>
		</div>
		<!-- 通用js -->
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<!-- 分页 -->
		<%@include file="/WEB-INF/views/common/page.jsp"%>
		<script>
			//查询方法
			$('#search').click(function(){
				//将分页的 的数据带过来
				$('#mainForm').append($('#pageForm').html());
				showLoading();//显示等待框
				$('#mainForm').submit();
			});
			//添加方法
			$('#add').click(function(){
				var para={
					url:"sysQuartz!sysQuartzForm.do",
					title:"添加",
					btnOK:"保存"
				};
				addOrUpdate(para);//通用新增修改方法
			});
			//修改方法
			$('[data-opt=edit]').click(function(){
				var para={
					url:"sysQuartz!sysQuartzForm.do",
					para:"id="+$(this).attr("data-id"),
					title:"修改",
					btnOK:"修改"
				};
				addOrUpdate(para);//通用新增修改方法
			});
			//删除方法
			$('#delete').click(function(){
				var dataIds = getdataIds("check");
				if(dataIds){
					layer.confirm('确认删除所选择的吗?', function(index){
						getJsonDataByPost("sysQuartz!delete.do","dataIds="+dataIds,function(data){
							if(data.result){
								layer.alert("删除成功!",function(){
									$('#search').click();//查询
								}); 
							}else{
								l.alert(data.msg);//错误消息弹出
							}
							layer.close(index);//关闭删除确认提示框
						},true); 
					});
				}
			});
		
		</script>
	</body>
</html>